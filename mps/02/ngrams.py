"""CS 331 MP2: ngram extractor and passage generator"""

import sys
import random
from pprint import pprint

def get_file_contents(filename):
    """Returns the contents of the specified file as a single string."""
    with open(filename, 'r') as f:
        content = f.read()
    return content

def compute_ngrams(str, n=2):
    """Returns an n-gram dictionary based on tokens from the given string.
    
    Each key in the dictionary will be a token from the string, and will map to
    a list of tuples of length (n-1) containing tokens that follow it in the
    string. For instance, the string "I really really like cake." will generate
    the following n-gram dictionary, with n=3:

        {'I': [('really', 'really')], 
         'really': [('really', 'like'), ('like', 'cake.')]}

    """
    pass

def gen_passage(ngrams, start=None, min_length=100):
    """Generates and returns a string based on the provided n-gram dictionary.
    
    The generated passage of text will start with either the provided start
    token or a randomly selected key from the dictionary, and the following text
    will be based on randomly generated tokens based on n-gram dictionary
    entries. After min_length is reached, the function will look for a place to
    gracefully end the passage --- e.g., after a token ending with a period.

    The following dictionary:

        {'I': [('really', 'really')], 
         'really': [('really', 'like'), ('like', 'cake.')]}

    May generate this passage, for instance (with a bit of programmed
    embellishment vis-a-vis punctuation and capitalization):

        I really really like. Really really like. Really really like. Really
        like cake. Really like cake. Really like cake.

    """
    pass

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: python', sys.argv[0], 'NGRAM-LENGTH FILENAME')
        sys.exit(0)
    
    ngram_len = int(sys.argv[1])
    filename = sys.argv[2]

    # Effectively a 1-gram demo; shows how to use `random.choice` and `str.join`
    # Delete when you start working on `gen_passage`!
    all_toks = get_file_contents(filename).split()
    passage = ' '.join(random.choice(all_toks) for _ in range(100))
    print(passage)

    ngrams = compute_ngrams(get_file_contents(filename), ngram_len)
    pprint(ngrams) # use for debugging (note: pretty-print for easy inspection)
    print(gen_passage(ngrams))
