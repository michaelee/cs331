from timeit import timeit
import random
import sort_algs

if __name__ == '__main__':
    # Delete or modify the following code to add your own implementation
    # that generates the required table of sort timings. Note that you
    # should also edit 'sort_algs.py' to add your own sort algorithm.

    for n_elems in range(10, 101, 10):
        l = list(range(n_elems))
        
        def call_insertion_sort():
            random.shuffle(l)
            sort_algs.insertion_sort(l)
            
        t = timeit(call_insertion_sort, number=1000)
        print('Time for n={n:<3} : {time:>5.3f}s'.format(n=n_elems, time=t))
