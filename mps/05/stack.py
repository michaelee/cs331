class Stack:
    def __init__(self):
        self.data = []

    def push(self, x):
        self.data.append(x)

    def pop(self):
        idx = len(self.data) - 1
        val = self.data[idx]
        del self.data[idx]
        return val

    def __repr__(self):
        return repr(self.data)

    def __len__(self):
        return len(self.data)

def check_parens(str):
    stack = Stack()
    for c in str:
        if c == '(':
            stack.push(c)
        elif c == ')':
            if stack:
                stack.pop()
            else:
                return False
    if stack:
        return False
    else:
        return True
