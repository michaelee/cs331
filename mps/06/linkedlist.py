class LinkedList:
    class Link:
        def __init__(self, val, prior=None, next=None):
            self.val = val
            self.prior = prior # makes this a "doubly-linked" list
            self.next = next

    def __init__(self):
        self.head = LinkedList.Link(None) # "sentinel" value
        self.head.next  = self.head
        self.head.prior = self.head
        self.count = 0

    def __normalize_index(self, idx):
        """Normalizes negative indexes to positive values"""
        if idx < 0:
            idx = idx + self.count
            if idx < 0:
                idx = 0
        return idx
            
    def append(self, x):
        l = LinkedList.Link(x, self.head.prior, self.head)
        self.head.prior.next = l
        self.head.prior = l
        self.count += 1
        
    def clear(self):
        """Removes all elements from the list"""
        pass
        
    def count(self, x):
        """Returns the number of x's in the list"""
        pass

    def extend(self, seq):
        """Appends all elements in seq to the list"""
        pass
        
    def insert(self, idx, x):
        """Inserts x into the list at idx"""
        if idx > self.count:
            raise IndexError
        pass
        
    def pop(self, idx=-1):
        """Deletes and returns item at idx"""
        pass

    def prepend(self, x):
        l = LinkedList.Link(x, self.head, self.head.next)
        self.head.next.prior = l
        self.head.next = l
        self.count += 1

    def remove(self, x):
        """Deletes the first instance of x; raises ValueError if not found"""
        raise ValueError    

    def __add__(self, other):
        """Supports lst1 + lst2"""
        pass

    def __contains__(self, x):
        """Supports x in lst"""
        return False

    def __delitem__(self, idx):
        """Supports del lst[idx]"""
        pass
    
    def __getitem__(self, idx):
        """Supports lst[i] (read)"""
        idx = self.__normalize_index(idx)
        if idx > self.count - 1:
            raise IndexError
        pass

    def __iter__(self):
        l = self.head.next
        while l is not self.head:
            yield l.val
            l = l.next

    def __len__(self):
        return self.count

    def __repr__(self):
        return repr(list(self))

    def __setitem__(self, idx, val):
        """Supports lst[i] = x"""
        idx = self.__normalize_index(idx)
        if idx > self.count - 1:
            raise IndexError
        pass
