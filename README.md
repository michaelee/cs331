# CS 331 Code Repository

This is the public repository for CS 331: Data Structures and Algorithms, which
contains sample code (some of which are used in lecture demos) and template code
for machine problems.

After cloning this repository, you should replace the contents of this README
file with, at minimum, your name, email address, and student ID.
