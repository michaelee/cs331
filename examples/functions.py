"""Misc definitions and calls used to demo functions in the intro slides"""

def mysum(x, y):
    return x+y # works for all plus-able things!

def mysum_v2(vals):
    accum = 0 # restricts to numbers
    for item in vals:
        accum += item
    return accum

print(mysum_v2([1, 2, 3, 4, 5]))
print(mysum_v2(range(10)))
# print(mysum_v2(['hello', 'world'])) # fails

def mysum_v3(vals, start):
    accum = start
    for item in vals:
        accum += item
    return accum

print(mysum_v3([1, 2, 3, 4, 5], 0))
print(mysum_v3(['hello', 'world'], ''))
print(mysum_v3([(1, 2), (), (5,)], ()))

def mysum_v4(vals, start=0):
    """This version defaults to using 0 for start"""
    accum = start
    for item in vals:
        accum += item
    return accum

print(mysum_v4([1, 2, 3, 4, 5]))
print(mysum_v4(['hello', 'world'], ''))
print(mysum_v4(['hello', 'world'], start=''))
print(mysum_v4(start='-->', vals=['a', 'b', 'c']))

def mysum_v5(*vals, start=0):
    """This version takes an arbitrary number of
    arguments that are automatically bundled into
    the vals variable."""
    accum = start
    for item in vals:
        accum += item
    return accum

print('mysum_v5')
print(mysum_v5(1, 2, 3, 4))
print(mysum_v5('hello', ' ', 'world', start='>'))
#print(mysum_v5(start='>', 'foo', 'bar')) # ordering restriction!
args = [10, 20, 30] + list(range(40, 110, 10))
print(mysum_v5(*args)) # "unpack" args from an existing list

def reduce(combiner, *vals, start=0):
    """Combines all items in vals with the provided
    combiner function and start value"""
    accum = start
    for item in vals:
        accum = combiner(accum, item)
    return accum

def add(m, n):
    return m+n

def mult(m, n):
    return m*n

print('reduce')
print(reduce(add, 1, 2, 3, 4))
print(reduce(add, 'hello', 'world', start=''))
print(reduce(mult, 1, 2, 3, 4))
print(reduce(mult, 1, 2, 3, 4, start=1))
print(reduce(mult, *range(1, 10), start=1))

add  = lambda m, n: m+n
mult = lambda m, n: m*n
pow_of_2 = lambda x: 2**x

print(add(5, 6))  # => 11
print(mult(5, 6)) # => 30
print(pow_of_2(10)) #=> 1024

print((lambda x, y: x**y)(2, 10))

print(reduce(lambda x,y: x*y, 1, 2, 3, 4, start=1))
print(reduce(lambda sos,n: sos + n**2, 1, 2, 3, 4))
print(reduce(lambda total, s: total + len(s),
             'hello', 'beautiful', 'world'))
print(reduce(lambda s, l: s & set(l), # set intersect
             range(0,10), range(5,20), range(8,12),
             start=set(range(0,100))))



def fibonacci(nth):
    a, b = 1, 1
    for _ in range(nth-1):
        a, b = b, a+b
    return a

print([fibonacci(i) for i in range(1, 15)])

def print_char_sheet(name,         # normal arg
                     *inventory,
                     race='Human',
                     **info):      # arbitrary keyword args in dict
    """Demonstrates all sorts of arg types."""
    print('Name: ', name)
    print('Race: ', race)
    print('Inventory:')
    for item in inventory:
        print(' -', item)
    for k in sorted(info.keys()):
        print('*', k, '=', info[k])

def test_else():
    for i in range(10):
        if i == 5:
            print('see 5')
    else:
        return False

print(test_else())

def i_take_3_args(foo, bar, baz):
    print('Got', foo, bar, baz)

i_take_3_args(1, 2, 3)              # positional args
i_take_3_args(baz=3, foo=1, bar=2)  # named args
args = {'bar': 2, 'baz': 3, 'foo': 1}
i_take_3_args(**args)               # unpacking args from dictionary
